package com.example.ngo1;

public class customer_list {

    private String address;
    private String item_summary;
    private String item_ID;
    private String contacts;



    public customer_list(String contacts, String ID , String data, String item) {
        this.address = data;
        this.item_summary = item;
        this.item_ID = ID;
        this.contacts = contacts;
    }

    public String getData() {
        return address;
    }

    public String getItem_summary() {
        return item_summary;
    }

    public String getItem_ID() {
        return item_ID;
    }

    public String getAddress() {
        return address;
    }

    public String getContacts() {
        return contacts;
    }
}
