package com.example.ngo1;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Region;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AcceptDialog extends AppCompatDialogFragment  {

    private EditText editTextDeliveryExecutiveName;
    private EditText editTextDeliveryExecutivePhoneNumber;
    private AcceptDialog Listener;
    String time,date,zone;
    TextView pickupDate;
    DatePickerDialog datePickUpDialog;
    TextView pickUpTime;
    private FirebaseFirestore mFirestore;
    public Context Lcontext;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Lcontext = getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.accept_change_layout,null);
        builder.setView(view).setTitle("Enter Pick up and Timing details").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String ExecutiveName = editTextDeliveryExecutiveName.getText().toString();
                String ExecutivePhoneNUmber = editTextDeliveryExecutivePhoneNumber.getText().toString();
                Log.i("executive name",ExecutiveName);
                Log.i("executive name",ExecutivePhoneNUmber);
                Log.i("executive time",time);
                Log.i("executive date",date);
                //Log.i("executive date",TimeZone);

                Map<String,String> usermap = new HashMap<>();


                usermap.put("deliveryManName",ExecutiveName);
                usermap.put("phoneNumber",ExecutivePhoneNUmber);
                usermap.put("date",pickupDate.getText().toString());
                usermap.put("time",pickUpTime.getText().toString());

                mFirestore = FirebaseFirestore.getInstance();

                mFirestore.collection("ngo feedback").add(usermap).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                        //test();
                        Toast.makeText(Lcontext,"Executive details have been sent, view it in History",Toast.LENGTH_SHORT).show();


                        //String key = documentReference.push().getKey();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String error = e.getMessage();
                        Toast.makeText(Lcontext,error,Toast.LENGTH_SHORT).show();
                    }
                });




            }




        });

        editTextDeliveryExecutiveName = view.findViewById(R.id.editTextExecutiveName);
        editTextDeliveryExecutivePhoneNumber = view.findViewById(R.id.editTextExecutivePhoneNumber);
        pickupDate = view.findViewById(R.id.pickUp_date);
        pickUpTime = view.findViewById(R.id.pickUp_time);

        pickupDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get the values for day of month , month and year from a date picker
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                // display the values by using a toast
                //Toast.makeText(getApplicationContext(), day + "\n" + month + "\n" + year, Toast.LENGTH_LONG).show();



                datePickUpDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                pickupDate.setText(date);

                            }
                        }, mYear, mMonth, mDay);
                datePickUpDialog.setTitle("Choose date");
                datePickUpDialog.show();

            }
        });



        pickUpTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if(selectedHour < 12) {
                            zone = "AM";
                        } else {
                            zone = "PM";
                        }

                        time = selectedHour + ":" + selectedMinute+" "+zone;
                        pickUpTime.setText(time);
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });





        return builder.create();
    }






}
