package com.example.ngo1;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class pager extends FragmentStatePagerAdapter {

    int tabCount;


    public pager (FragmentManager fm,int numOfTabs)
    {
        super(fm);
        this.tabCount = numOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                 return new FragmentOrder();
            case 1:
                return  new FragmentRemovedOrder();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {

        return tabCount;
    }
}
