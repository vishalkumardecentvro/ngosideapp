package com.example.ngo1;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

public class RemoveDialog extends AppCompatDialogFragment implements AdapterView.OnItemSelectedListener {

    private com.example.ngo1.RemoveDialog listener;
    String spinnerReasonText;
    int check=0;

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.remove_order_layout,null);
        builder.setView(view).setTitle("Please select Reason for Deleting order ").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {





                //Listener.applyTexts(ExecutiveName,ExecutivePhoneNUmber);

            }
        });

        Spinner reason = view.findViewById(R.id.RemoveSpinner);



        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),R.array.Reason,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_gallery_item);
        reason.setAdapter(adapter);
        reason.setOnItemSelectedListener(this);




        return builder.create();
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        if(++check>1)
        {
            String text = adapterView.getItemAtPosition(i).toString();
            spinnerReasonText = text;
            Log.i("executive reason",spinnerReasonText);


        }



    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }





}
