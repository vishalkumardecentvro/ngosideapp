package com.example.ngo1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class ngoEvent extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    TextView eventDate;
    TextView eventTime;
    DatePickerDialog datePickerDialog;
    Button setEvent;
    EditText personName;
    String zone, time , date, eventType;
    private FirebaseFirestore gFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ngo_event);

        Spinner eventsList = findViewById(R.id.spinner_event);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.Event,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_gallery_item);
        eventsList.setAdapter(adapter);
        eventsList.setOnItemSelectedListener(this);

        eventDate = findViewById(R.id.event_date);
        eventTime = findViewById(R.id.event_time);
        setEvent = findViewById(R.id.set_event);
        personName = findViewById(R.id.candidate_EditText);

        eventDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get the values for day of month , month and year from a date picker
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                // display the values by using a toast
                //Toast.makeText(getApplicationContext(), day + "\n" + month + "\n" + year, Toast.LENGTH_LONG).show();



                datePickerDialog = new DatePickerDialog(ngoEvent.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                eventDate.setText(date);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.setTitle("Choose date");
                datePickerDialog.show();

            }
        });

        eventTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(ngoEvent.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if(selectedHour < 12) {
                            zone = "AM";
                        } else {
                            zone = "PM";
                        }

                        time = selectedHour + ":" + selectedMinute+" "+zone;
                        eventTime.setText(time);
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });


        setEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String,String> usermap = new HashMap<>();


                usermap.put("EventType",eventType);
                usermap.put("PersonName",personName.getText().toString());
                usermap.put("Time",time);
                usermap.put("Date",date);

                gFirestore = FirebaseFirestore.getInstance();

                gFirestore.collection("ngoEventData").add(usermap).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                        Toast.makeText(getApplicationContext(),"Event has been set up, you can view it in History section",Toast.LENGTH_LONG).show();

                        //test();

                        //String key = documentReference.push().getKey();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String error = e.getMessage();
                        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                    }
                });



            }


        });


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        String text = adapterView.getItemAtPosition(i).toString();
        eventType = text;

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}