package com.example.ngo1;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentOrder#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentOrder extends Fragment {

    String TAG = "FragmentOrder";
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mrecyclerView;
    ArrayList<acceptItemData> accept_list = new ArrayList<>();


    private acceptAdapter mAdapter;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();
    private CollectionReference ngo_feedback = mFirestore.collection("ngo feedback");

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentOrder() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentOrder.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentOrder newInstance(String param1, String param2) {

        FragmentOrder fragment = new FragmentOrder();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        //mfirestore = FirebaseFirestore.getInstance();

        dataFetchFromFirebase();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_order, container, false);

        mrecyclerView = (RecyclerView) rootView.findViewById(R.id.acceptRecyclerView);
        mLayoutManager = new LinearLayoutManager(getContext());
        return rootView;
    }


    public void dataFetchFromFirebase()
    {
        ngo_feedback.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    acceptItemData order_detail = documentSnapshot.toObject(acceptItemData.class);
                   // customer_detail.setOrder_id(documentSnapshot.getId());

                   // String order_id = customer_detail.getOrder_id();


                    //String item_name = customer_detail.getTypeOfItem();


                    //String item_rating = customer_detail.getRating();


                    String PickupName = order_detail.getDeliveryManName();

                    String PickupNumber = order_detail.getPhoneNumber();

//                    String[] NameArray = item_name.split("\\s+");
//                    String[] RateArray = item_rating.split("\\s+");
//                    String[] QtyArray = item_quantity.split("\\s+");




                    Log.i("del name ", PickupName);
                    Log.i("del number ", PickupNumber);
//                    Log.i("item Rate ", item_rating);
//                    Log.i("item address ", item_address);
////
//                    for(k=0;k<NameArray.length;k++)
//                    {
//                        dataField = dataField+NameArray[k]+"\t\tRating ="+ RateArray[k]+"\t\t Quantity ="+QtyArray[k]+"\n";
//
//                    }
//
//                    customerAddress.add(new customer_list(order_id,item_address,dataField));
//
//
//                    //dataField = item_name+" Rating= "+item_rating+" Qty= "+item_quantity;
//                    //customerAddress.add(new customer_list(item_address,dataField));
//

//                    dataField = "";

                    accept_list.add(new acceptItemData(PickupName,PickupNumber));
                    placeData();




                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
               // Toast.makeText(FragmentOrder.this, "Error loading information", Toast.LENGTH_SHORT).show();
                Log.d(TAG, e.getMessage());

            }
        });


    }

    public void placeData()
    {

        mrecyclerView.setHasFixedSize(true);


        mAdapter = new acceptAdapter(accept_list);
        mrecyclerView.setLayoutManager(mLayoutManager);
        mrecyclerView.setAdapter(mAdapter);



    }
}