package com.example.ngo1;

class acceptItemData {

    private String deliveryManName;
    private String phoneNumber;

    public acceptItemData(String deliveryManName, String phoneNumber) {
        this.deliveryManName = deliveryManName;
        this.phoneNumber = phoneNumber;
    }

    public acceptItemData() {

    }



    public String getDeliveryManName() {
        return deliveryManName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}


