package com.example.ngo1;

import com.google.firebase.database.Exclude;

public class customer_orders {

    private String typeOfItem;
    private String rating;
    private String quantity;
    private String address;
    private String donarNameNumberAlternate;
    private String id;


    public customer_orders (String address,String contact,String orderId,String item_type,String item_rating,String item_qty)
    {
        typeOfItem=item_type;
        rating = item_rating;
        quantity = item_qty;
        this.address = address;
        this.id = orderId;
        this.donarNameNumberAlternate = contact;

    }


    public String getAddress() {
        return address;
    }


    public customer_orders ()
    {
        // firestore always need empty constructor
    }


    public String getTypeOfItem() {
        return typeOfItem;
    }

    public String getRating() {
        return rating;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getDonarNameNumberAlternate() {
        return donarNameNumberAlternate;
    }

    public String getId() {
        return id;
    }
}
