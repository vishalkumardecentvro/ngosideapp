package com.example.ngo1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class donation_Request extends AppCompatActivity {

    TextView newview;
    Spinner spin;
    RecyclerView recyclerView;

    static ArrayList<String> listItems = new ArrayList<String>();
    static ArrayList<String> RatingList = new ArrayList<String>();
    static ArrayList<String> QtyList = new ArrayList<String>();
    static ArrayList<String> add = new ArrayList<String>();


    ArrayList<customer_list> customerAddress = new ArrayList<>();


    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mrecyclerView;
    private customAdapter mAdapter;
    private Context mContext;
    private FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();
    private CollectionReference ngo_order = mFirestore.collection("orders");
    String TAG = "donation_Request";
    TextView showItem;
    String dataField="";

    int i=0,k,l;

    //FirebaseAuth fauth;
    FirebaseFirestore mfirestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation__request);

        mfirestore = FirebaseFirestore.getInstance();
        //spin = (Spinner) findViewById(R.id.spinner);
        mrecyclerView = findViewById(R.id.recyclerView);
        mContext = this;
        //Spinner hour = findViewById(R.id.hourSpinner);


        dataFetch();
        placeData();

//        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.hour,android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_gallery_item);
//        hour.setAdapter(adapter);
        //hour.setOnItemSelectedListener(this);






    }

    public void dataFetch()
    {
        ngo_order.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    customer_orders customer_detail = documentSnapshot.toObject(customer_orders.class);

                    String order_id = customer_detail.getId();


                    String item_name = customer_detail.getTypeOfItem();


                    String item_rating = customer_detail.getRating();


                    String item_quantity = customer_detail.getQuantity();

                    String contactDetail = customer_detail.getDonarNameNumberAlternate();

                    String item_address = customer_detail.getAddress();
                    Log.i("item name ", item_name);
                    Log.i("item qty ", item_quantity);
                    Log.i("item Rate ", item_rating);
                    Log.i("item address ", item_address);


                    String[] NameArray = item_name.split("\\s+");
                    String[] RateArray = item_rating.split("\\s+");
                    String[] QtyArray = item_quantity.split("\\s+");






                    for(k=0;k<NameArray.length;k++)
                    {
                        dataField = dataField+NameArray[k]+"\t\t"+ RateArray[k]+"\t\t"+QtyArray[k]+"\n";

                    }

                    customerAddress.add(new customer_list(contactDetail,order_id,item_address,dataField));


                    //dataField = item_name+" Rating= "+item_rating+" Qty= "+item_quantity;
                    //customerAddress.add(new customer_list(item_address,dataField));

                    placeData();
                    dataField = "";




                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(donation_Request.this, "Error loading information", Toast.LENGTH_SHORT).show();
                Log.d(TAG, e.getMessage());

            }
        });


    }

    public void placeData()
    {
        mrecyclerView = findViewById(R.id.recyclerView);
        mrecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);

        mAdapter = new customAdapter(customerAddress);
        mrecyclerView.setLayoutManager(mLayoutManager);
        mrecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new customAdapter.OnItemClicklistener() {
            @Override
            public void onAcceptClick(int position) {

                openAcceptDialog();

            }

            @Override
            public void onRemoveClick(int position) {

                openRemoveDialog();

            }
        });


    }

    public void openAcceptDialog() // this opens dialog correcting values class
    {
        AcceptDialog executiveDialog = new AcceptDialog();
        executiveDialog.show(getSupportFragmentManager(),"Edit example dialog");
    }

    public void openRemoveDialog() // this opens dialog correcting values class
    {
        RemoveDialog deleteDialog = new RemoveDialog();
        deleteDialog.show(getSupportFragmentManager(),"Edit example dialog");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.common_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.acceptMenuselection:
                startActivity(new Intent(donation_Request.this,acceptedOrder.class));
                break;


            case R.id.EventMenuSelection:
                startActivity(new Intent(donation_Request.this,ngoEvent.class));
                break;


            default:
                return false;

        }
        return true;
    }
}
