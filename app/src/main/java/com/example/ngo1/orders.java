package com.example.ngo1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

public class orders extends AppCompatActivity {

    Button next;
    TextView click;
    int k=0;
    EditText inputEmail,inputPassword;
    FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

       inputEmail = findViewById(R.id.emailEditTextView);
        inputPassword = findViewById(R.id.passwordEditTextView);


        next = findViewById(R.id.button);
        click = findViewById(R.id.changeTextView);
        mAuth = FirebaseAuth.getInstance();

        if(mAuth.getCurrentUser()!=null ) // to keep user logged in when he has not logged out
        {

            startActivity(new Intent(orders.this,donation_Request.class));
            finish();
        }


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = inputEmail.getText().toString().trim();
                final String password = inputPassword.getText().toString().trim();
                if(TextUtils.isEmpty(email))
                {
                    Toast.makeText(getApplicationContext(),"Enter email address ",Toast.LENGTH_LONG).show();
                    inputEmail.requestFocus();
                    return;

                }
                if(TextUtils.isEmpty(password))
                {
                    Toast.makeText(getApplicationContext(),"Enter password ",Toast.LENGTH_LONG).show();
                    inputPassword.requestFocus();
                    return;
                }
                if(password.length()<6)
                {
                    Toast.makeText(getApplicationContext(),"password too short enter minimum 6 characters  ",Toast.LENGTH_LONG).show();
                    return;

                }
                if(k==0) {

                    mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(orders.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (!task.isSuccessful()) {
                                Toast.makeText(orders.this, "Authentication failed ", Toast.LENGTH_LONG).show();

                            }
                            if (task.isSuccessful()) {
                                mAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful())
                                        {
                                            Toast.makeText(orders.this, " you are registered...please Check your email for verification ", Toast.LENGTH_LONG).show();
                                            next.setText("Log In");
                                            click.setText("Not registered? Click here to register ");
                                            k=1;
                                            //finish();

                                        }
                                        else
                                        {
                                            Toast.makeText(orders.this,task.getException().getMessage(), Toast.LENGTH_LONG).show();


                                        }

                                    }
                                });



                            }

                        }
                    });
                }
                else if(k==1)
                {
                    mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(orders.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful())
                            {
                                if(password.length()<6)
                                {
                                    inputPassword.setError("password minimun length should be more than 6 characters");

                                }
                                else
                                {
                                    Toast.makeText(orders.this,"Incorrect username or password", Toast.LENGTH_LONG).show();
                                    inputPassword.setText("");
                                }
                            }
                            else
                            {
                                if(mAuth.getCurrentUser().isEmailVerified())
                                {
                                    Intent intent = new Intent(orders.this,donation_Request.class);
                                    startActivity(intent);
                                }
                                else
                                {
                                    Toast.makeText(orders.this,"Please verify your email address",Toast.LENGTH_SHORT).show();
                                }
                            }

                        }
                    });

                }


            }
        });




    }


    public void register (View view)
    {
        if(k==0)
        {
            next.setText("Log In");
            click.setText("Not registered? Click here to register ");
            k=1;

        }
        else if (k==1)
        {
            next.setText("Register");
            click.setText("Already registered? Click here to Log In ");
            k=0;

        }

        //startActivity(new Intent(getApplicationContext(),donation_Request.class));


    }


}


