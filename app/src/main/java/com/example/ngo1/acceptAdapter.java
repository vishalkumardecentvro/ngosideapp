package com.example.ngo1;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class acceptAdapter extends RecyclerView.Adapter<acceptAdapter.exampleViewHolder > {

    ArrayList<acceptItemData> mList = new ArrayList<>();



    @NonNull
    @Override
    public acceptAdapter.exampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.accepted_orders_history ,parent,false );
        acceptAdapter.exampleViewHolder evh = new acceptAdapter.exampleViewHolder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull acceptAdapter.exampleViewHolder holder, int position) {

        acceptItemData currentItem = mList.get(position);
        holder.namePickUp.setText(currentItem.getDeliveryManName());
        holder.numberPickUp.setText(currentItem.getPhoneNumber());

    }


    public acceptAdapter(ArrayList<acceptItemData> mList) {
        this.mList = mList;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class exampleViewHolder extends RecyclerView.ViewHolder
    {

        public TextView acceptID;
        public TextView namePickUp;
        public TextView numberPickUp;



        public exampleViewHolder(@NonNull View itemView) {
            super(itemView);

            acceptID = itemView.findViewById(R.id.order_ID);
            namePickUp = itemView.findViewById(R.id.exeNameTextView);
            numberPickUp = itemView.findViewById(R.id.exeNumberTextView);

        }
    }

}
