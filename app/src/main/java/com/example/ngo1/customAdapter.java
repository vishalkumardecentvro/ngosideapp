package com.example.ngo1;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class customAdapter extends RecyclerView.Adapter<customAdapter.ExampleViewHolder > {

    private ArrayList<customer_list> mExampleList;
    private OnItemClicklistener mListener;

    public interface OnItemClicklistener
    {
        void onAcceptClick(int position);
        void onRemoveClick(int position);

    }


    public void setOnItemClickListener(OnItemClicklistener Listener)
    {
        mListener = Listener;
    }


    public static class ExampleViewHolder extends RecyclerView.ViewHolder
    {
        public TextView item_add;
        public TextView item_sum;
        public TextView Item_ID;
        public Button acceptItm;
        public Button removeItm;



        public ExampleViewHolder(@NonNull View itemView,final OnItemClicklistener Listener) {
            super(itemView);
            item_add = itemView.findViewById(R.id.address_text);
            item_add.setMovementMethod(new ScrollingMovementMethod());
            item_sum = itemView.findViewById(R.id.itemSummary_text);
            item_sum.setMovementMethod(new ScrollingMovementMethod());
            Item_ID = itemView.findViewById(R.id.order_ID);
            acceptItm = itemView.findViewById(R.id.AcceptCard);
            removeItm = itemView.findViewById(R.id.RemoveCard);

            removeItm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(Listener != null)
                    {
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION)
                        {

                            //openDialog();
                            Listener.onRemoveClick(position);

                        }
                    }

                }
            });

            acceptItm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(Listener != null)
                    {
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION)
                        {

                            //openDialog();
                            Listener.onAcceptClick(position);

                        }
                    }

                }
            });

        }
    }

    public customAdapter (ArrayList<customer_list> exampleList)
    {
        mExampleList = exampleList;

    }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_layout ,parent,false );
        ExampleViewHolder evh = new ExampleViewHolder(v,mListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {

        customer_list currentItem = mExampleList.get(position);
        holder.item_add.setText(currentItem.getContacts()+"\n");
        holder.item_add.append(currentItem.getData());
        holder.item_sum.setText(currentItem.getItem_summary());
        holder.Item_ID.setText(currentItem.getItem_ID());

    }

    @Override
    public int getItemCount() {
        return mExampleList.size();
    }
}
