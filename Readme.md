Idea -> In this android app we are listing out all the donation items which are to be donated from donors end. This is a NGO  side application and will be use by NGO workers. They can accept the item remove item with providing a suitable reason. NGO workers can set up the pick up schedule and they can also set events like birthday, wedding and invite the people who are intrested.

<img src="imagesRepo/Ngo-app-merchant-side-part-1.gif" width = "250">
<img src="imagesRepo/1_DonationItemLayout.png" width = "250">
<img src="imagesRepo/2_pickupSchedule.png" width = "250">
<img src="imagesRepo/3_DeleteOrder.png" width = "250">
<img src="imagesRepo/4_DeleteReason.png" width = "250">
<img src="imagesRepo/5_NgoEvents.png" width = "250">
<img src="imagesRepo/6_eventDate.png" width = "250">
<img src="imagesRepo/7_eventTime.png" width = "250">